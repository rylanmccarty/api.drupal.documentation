# How-to Guides

## Table of Contents

* Site navigation
  * [README](/README.md)
  * [explanation TOC](/explanation/README.md)
  * [how-to guides TOC](/how-to%20guides/README.md)
  * [reference TOC](/reference/README.md)
  * [tutorials TOC](/tutorials/README.md)
* In this directory
  * account management
    * [Create a User Account](/how-to%20guides/account%20management.md##-Create-a-User-Account)
    * [Create a Role](/how-to%20guides/account%20management.md##-Create-a-Role)
    * [Assing Permissions to a Role](/how-to%20guides/account%20management.md##-Assign-Permissions-to-a-Role)
    * [Change a User's Role](/how-to%20guides/account%20management.md##-Change-a-User's-Roles)
    * [Resources](/how-to%20guides/account%20management.md##-Resources)
  * api management
    * [Create API documentation](/how-to%20guides/api%20management.md##-Create-API-documentation)
    * [Edit API documentation](/how-to%20guides/api%20management.md##-Edit-API-documentation)
    * [Delete API documentation](/how-to%20guides/api%20management.md##-Delete-API-documentation)
    * [Create and manage API catagories](/how-to%20guides/api%20management.md##-Create-and-manage-API-catagories)
    * [Control access to your API docs](/how-to%20guides/api%20management.md##-Control-access-to-your-API-docs)
  * content management
    * [Edit the landing page](/how-to%20guides/content%20management.md##-Edit-the-landing-page)
    * [Create a basic page](/how-to%20guides/content%20management.md##-Create-a-basic-page)
    * [Create an FAQ](/how-to%20guides/content%20management.md##-Create-an-FAQ)
    * [Manage your blog](/how-to%20guides/content%20management.md##-Manage-your-blog)
    * [Administer a Forum](/how-to%20guides/content%20management.md##-Administer-a-Forum)
  * developer management
