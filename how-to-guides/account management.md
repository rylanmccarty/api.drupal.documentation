# Account Management

## Table of Contents

* Site navigation
  * [README](/README.md)
  * [explanation TOC](/explanation/README.md)
  * [how-to guides TOC](/how-to%20guides/README.md)
  * [reference TOC](/reference/README.md)
  * [tutorials TOC](/tutorials/README.md)
* In this document
  * [Create a User Account](##-Create-a-User-Account)
  * [Create a Role](##-Create-a-Role)
  * [Assign Permissions to a Role](##-Assign-Permissions-to-a-Role)
  * [Change a User's Role](##-Change-a-User's-Roles)
  * [Resources](##-Resources)
  
## Preface

Anyone who visits your website is a user. There are three groups of users:

* Users who a not logged in, or *anonymous users* (External Public)
* Users who are logged in, or *authenticated users* (Internal Private)
* The administrative user account that was automatically created when you installed your site.

The ability to do actions on your site (including viewing content, editing content, and changing configuration) is governed by **permissions**. Each permission has a name. A user must be granted a permission in order to do the corresponding action on the site; permissions are defined by the modules that provide the actions.

Rather than assigning individual permissions directly to each user, permissions are grouped into **roles**. You can define one or more roles on your site, and then grant permissions to each role. The permissions granted to authenticated and anonymous users are contained in the *Authenticated user* and *Anonymous user* roles, and depending on the installation profile you used when you installed your site, there may also be an *Administrator* role that is automatically assigned all permissions on your site.

## Create a User Account

1. In the *Manage* administrative menu, navigate to People (admin/people)
2. Click Add user.

## Create a Role

Desired roles: **anonymous** and **internal private**

1. In the *Manage* administrative menu, navigate to People > Roles (admin/people/roles). ![roles o overview](./static/roles.png)
2. You will find the default roles *Anonymous user*, *Authenticated user*, and *Administrator* already present.
3. Click *Add Role* to add a custom role.

## Assign Permissions to a Role

1. In the *Manage* administrative menu, navigate to *People > Roles* (admin/people/roles). The *Roles* page appears.
2. Click *Edit permissions* in the dropdown for the *Authenticated user* (If you don't select the arrow dropdown first and instead just click *Edit* you will be taken to the wrong screen) ![edit permissions](./static/edit%20permissions.png)
3. Click *Save permissions* to persist the changes.

## Change a User's Roles

1. In the *Manage* administrive menu, navigate to *People* (admin/people)
2. Locate the desired user
3. Click *Edit* and scroll down to the *Roles* section. Check/Uncheck desired roles.
4. Click *Save* to persist the changes.

## Resources

* <https://www.drupal.org/node/120614>
* <https://www.drupal.org/docs/7/managing-users>
* <https://www.drupal.org/node/1803614>
