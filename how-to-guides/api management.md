# API Management

## Table of Contents

* Site navigation
  * [README](/README.md)
  * [explanation TOC](/explanation/README.md)
  * [how-to guides TOC](/how-to%20guides/README.md)
  * [reference TOC](/reference/README.md)
  * [tutorials TOC](/tutorials/README.md)
* In this document
  * [Create API documentation](##-Create-API-documentation)
  * [Edit API documentation](##-Edit-API-documentation)
  * [Delete API documentation](##-Delete-API-documentation)
  * [Create and manage API catagories](##-Create-and-manage-API-catagories)
  * [Control access to your API docs](##-Control-access-to-your-API-docs)

## Create API documentation

| Permission Name | Min. Role Required |
| ---- | ---- |
| Apigee API Catalog > Create new API docs | API Moderator |

The Kickstart distribution include the Apigee API Catalog module, which lets your document your APIs in the portal. The API Catalog module transforms valid OpenAPI Specifications in JSON or YAML into SmartDocs documentation.

1. Login with an account that has equal or more permissions than the specified minimum role required.
2. Navigate to *Content > Add API doc*. ![add doc](./static/add%20doc.png)
3. Upload your OpenAPI specification in either JSON or YAML. Provide a brief description and assign the API catagory which is a pre-defined drupal taxonomy. *You can view the predefined catagories by navigating to the API Catalog page.* ![api catagories](./static/api%20catagories.png)

## Edit API documentation

| Permission Name | Min. Role Required |
| ---- | ---- |
| Apigee API Catalog > Edit API docs | API Moderator |

1. Login with an account that has equal or more permissions than the specified minimum role required.
2. Navigate to *Content > API catalog* ![api catalog](./static/api%20catalog.png)
3. Click *Edit* on the corresponding API and make your changes
4. Click *Save* to persist changes

## Delete API documentation

| Permission Name | Min. Role Required |
| ---- | ---- |
| Apigee API Catalog > Delete API docs | API Moderator |

1. Follow steps 1-3 in [## Edit API documentation](##-Edit-API-documentation)
2. Click on the *Delete* tab.
3. Click *Delete* to finalize the process. ![delete](./static/delete%20doc.png)

## Create and manage API catagories

| Permission Name | Min. Role Required |
| ---- | ---- |
| Taxonomy > API Category: Create terms | API Moderator |
| Taxonomy > API Category: Delete terms | API Moderator |
| Taxonomy > API Category: Edit terms | API Moderator |
| Taxonomy > Access the taxonomy voca... | API Moderator |

1. Login with an account that has equal or more permissions than the specified minimum role required.
2. Navigate to *Structure > Taxonomy > API Catagory* ![api catagories](./static/api%20taxonomy.png)
3. From there you have a few options:

* Add new category
  * Click *+ Add term*
  * Provide a Name and brief description
  * Click *Save* to persist changes
* Edit existing category
  * Click *Edit* on the desired category
* Delete existing category
  * Click on the *Edit* dropdown arrow and select *Delete*

## Control access to your API docs

You can allow anonymous users to view published API documentation pages in your developer portal, or restrict documentation to registered users, by controlling access through user permissions. To enable or restrict access:

1. Log in to the portal as a user with *administrator* privileges.
2. Navigate to *People > Permissions > Apigee API Catalog > View published API docs* in the Drupal admin menu.
3. Select the desired permissions for each user role.
4. Click *Save* to persist the changes.

## Resources

<https://www.drupal.org/docs/8/modules/apigee-api-catalog>
<https://www.drupal.org/docs/8/modules/apigee-api-catalog/expose-rest-apis-to-interact-with-api-docs>