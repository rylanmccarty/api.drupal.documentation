# Developer management

## Deactivate developers

You have two options:

1. Deactivate a developer on the developer portal (recommended). In this case, the developer will not be able to log into the developer portal, and assigned API keys will be deactivated and no longer work.
2. Deactivate a developer on Apigee Edge. The developer will be able to log into the developer portal, but assigned API keys will be deactivated and no longer work. The developer will be notified about the deactivated API keys in the My Apps page.

To disable a developer on the developer portal:

1. Click **People** in the top menu bar.
2. Click **Edit** in the Operations column associated with the developer you want to block.
3. On the Edit page, under Status, click **Blocked**.
4. Click **Save**.