# Content Management

## Table of Contents

* Site navigation
  * [README](/README.md)
  * [explanation TOC](/explanation/README.md)
  * [how-to guides TOC](/how-to%20guides/README.md)
  * [reference TOC](/reference/README.md)
  * [tutorials TOC](/tutorials/README.md)
* In this document
  * [Edit the landing page](##-Edit-the-landing-page)
  * [Create a basic page](##-Create-a-basic-page)
  * [Create an FAQ](##-Create-an-FAQ)
  * [Manage your blog](##-Manage-your-blog)
  * [Administer a Forum](##-Administer-a-Forum)

## Edit the landing page

| Permission Name | Min. Role Required |
| ---- | ---- |
| Apigee API Catalog > Edit API docs | API Moderator |

1. Login with an account that has equal or more permissions than the specified minimum role required.

## Create a basic page

| Permission Name | Min. Role Required |
| ---- | ---- |
| Apigee API Catalog > Edit API docs | API Moderator |

1. Login with an account that has equal or more permissions than the specified minimum role required.
2. Navigate to *Content > Add content > Basic Page*

## Create an FAQ

| Permission Name | Min. Role Required |
| ---- | ---- |
| Apigee API Catalog > Edit API docs | API Moderator |

1. Login with an account that has equal or more permissions than the specified minimum role required.

## Manage your blog

| Permission Name | Min. Role Required |
| ---- | ---- |
| Apigee API Catalog > Edit API docs | API Moderator |

1. Login with an account that has equal or more permissions than the specified minimum role required.

## Administer a Forum

| Permission Name | Min. Role Required |
| ---- | ---- |
| Apigee API Catalog > Edit API docs | API Moderator |

1. Login with an account that has equal or more permissions than the specified minimum role required.

## Update Hero banner