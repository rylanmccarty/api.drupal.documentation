# Reference

## Table of Contents

* Site navigation
  * [README](/README.md)
  * [explanation TOC](/explanation/README.md)
  * [how-to guides TOC](/how-to%20guides/README.md)
  * [reference TOC](/reference/README.md)
  * [tutorials TOC](/tutorials/README.md)
* In this directory
  * [drupal architecture](drupal%20architecture.md)
  * [drupal integrations](drupal%20integrations.md)
  * [site structure](site%20structure.md)
  * [example portals](example%20portals.md)