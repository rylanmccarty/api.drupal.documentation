# Drupal Integrations

This document illustrates how Drupal interacts with Apigee Edige. Additionally, the goal is to show how Apigee products work together with applications.

![drupal integration diagram](./static/drupal%20integration%20diagram.png)

**Applications** 