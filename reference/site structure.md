# Site structure

## Table of Contents

* Site navigation
  * [README](/README.md)
  * [explanation TOC](/explanation/README.md)
  * [how-to guides TOC](/how-to%20guides/README.md)
  * [reference TOC](/reference/README.md)
  * [tutorials TOC](/tutorials/README.md)
* In this file
  * [Main site content](##-Main-site-content)
  * [Administration content](##-Administration-content)

---
![drupal kickstart structure](./static/kickstart%20site%20structure.png)

This is a site structure for a 1st stage developer portal maturity level. It functions as the backbone for building a more mature developer portal platform.

There are two major components to a Drupal portal, and they are the main site content (what user sees) and the administrator content.

## Main site content

* Home - Landing page and responsible for communicating to developers, tech executives, and stakeholders. The goal is to orient developers and then guide them to a quick onboarding process. For executives and stakeholders the goal is to illustrate some of your technical capabilities and market your product. A clean and well-polished landing page can do wonders for developer relations and fostering trust.
* APIs - Listing of all API specifications that is uploaded to the developer portal.
* Get started - A key page for developer onboarding. This page will contain information for guiding the developer through the registration process, site-specific guides like registering an application/provisioning API keys, dealing with authorization/authentication, etc.
* Blog - The blog serves multiple functions and can be used in a few differnt ways. The first way is through conveying the idea of active API development (using the blog as the place to describe API changes). Your site is also the face of your API program so it can be used as a place to place program updates.
* Forum - typically a feature of more mature portal programs, the forum is the place to start fostering a community and allow a public support engagement. There are many ways to foster a community, e.g. stack-overflow, slack, discord, etc.
* FAQs - Site-wide questions and answers
* Apps - User-specific area to manage their developer applicaitons. You can view API keys and secrets, what Apigee products are provisioned for a specific application, app analytics, and can create/delete applications.
* Teams - The only way to manage developer teams if using apigee for private cloud. Teams allow developers (portal users) to share responsibility for an app with other developers. Apps that are owned by a team are accessible to all members of the team based on their developer teams role.

## Administration content

There are a lot of options for administrator and site-builders to create content, modify existing content, and build complex imformation architectures. The most important areas are color coated based on functionality.

* Green highlighted areas are mainly used for creating and managing content. This content typically follows a pre-defined structure that a site-builder defines in advance. The API catalog will be the most important area to go when publishing API documentation.
* Red highlighted areas are where site-builders will usually stay. These pages help to define how the content will be structured, filtered, and viewed. API catagories (used for filtering APIs) are a type of Taxonomy and will be important for API documentation.
* Yellow highlighted areas are for administrative functions like managing permissions, roles, and portal users.