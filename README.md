# Developer portal documentation

![Photo by NASA on Unsplash](./static/nasa-yZygONrUBe8-unsplash.jpg)

---

## Table of Contents

* Site navigation
  * [README](./README.md)
  * [Explanation TOC](./explanation/README.md)
  * [How-to guides TOC](/how-to-guides/README.md)
  * [Reference TOC](/reference/README.md)
  * [Tutorials TOC](/tutorials/README.md)
* In this document
  * [How this documentation is structured](##-How-this-documentation-is-structured)

## How this documentation is structured

This documentation is structured around **four different functions** to be used at different times and in different circumstances:

1. A **tutorial** is *learning-oriented*, allows a newcomer to get started, and is a lesson.
2. A **how-to guide** is *goal-oriented*, shows how to solve a specific problems, and usually contains a series of steps
3. An **explanation** is *understanding-oriented*, explains a concept, and provides background information and context around a particular decision or technology.
4. A **reference** is *information-oriented*, describes the machinery, and is accurate and complete.
